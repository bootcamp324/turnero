
# Turnero | Gestionador de turnos

Turnero es una aplicación diseñada para gestionar turnos de manera eficiente y sencilla. Este proyecto se enfoca en ofrecer una solución para cualquier tipo de negocio que requiera de la organización de sus clientes a través de citas o turnos.

La app está dirigida tanto a pequeñas como a grandes empresas, consultorios médicos, peluquerías, spa, restaurantes, entre otros, que necesiten una herramienta para administrar sus citas y mejorar su servicio al cliente.


## Funcionalidades

- Registro de clientes y empleados
- Asignación de turnos según su nivel de prioridad
- Visualización en vivo de los turnos actuales por cola
- Creación de diferentes colas según los servicios disponibles
- Panel de administración para gestionar los diferentes datos
- Gestión de usuarios y permisos


## Tecnologías utilizadas

- Python
- Django
- HTML 5
- CSS 3
- JavaScript
- Bootstrap 5
- PostgreSQL


## Instalación

Clonar el repositorio del proyecto

    git clone https://gitlab.com/bootcamp324/turnero.git

Ir a la carpeta del proyecto

    cd turnero

Crear el entorno virtual

    python -m venv venv

Iniciar el entorno virtual

    venv\scripts\activate

Instalar los requerimientos del proyecto

    pip install -r requirements.txt

Entrar a la carpeta del sitio

    cd mysite

Modificar settings.py para conectar con tu base de datos PostgreSQL

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': '<nomobre de tu base de datos>',
            'USER': '<tu usuario>',
            'PASSWORD': '<tu contraseña>',
            'HOST': '127.0.0.1',
            'PORT': '5432',
        }
    }

Realizar las migraciones correspondientes

    python manage.py makemigrations
    
    python manage.py migrate

Colectar los archivos estáticos

    python manage.py collectstatic

(Opcional) Cargar los datos de prueba de la base de datos del proyecto

    python manage.py loaddata datos_turnero.json

Iniciar el servidor

    python manage.py runserver

Entrar a la url localhost:8000
    

## Autor

- [@Hezqiel](https://gitlab.com/Hezqiel)
CREATE SEQUENCE public.permiso_permiso_id_seq;

CREATE TABLE public.permiso (
                permiso_id SMALLINT NOT NULL DEFAULT nextval('public.permiso_permiso_id_seq'),
                descripcion VARCHAR(20) NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT permiso_id PRIMARY KEY (permiso_id)
);
COMMENT ON TABLE public.permiso IS 'Tabla de registros de los permisos';
COMMENT ON COLUMN public.permiso.permiso_id IS 'PRIMARY KEY Identificador del permiso';
COMMENT ON COLUMN public.permiso.descripcion IS 'Descripci�n del permiso';
COMMENT ON COLUMN public.permiso.estado IS 'Estado del permiso';


ALTER SEQUENCE public.permiso_permiso_id_seq OWNED BY public.permiso.permiso_id;

CREATE SEQUENCE public.prioridad_prioridad_id_seq;

CREATE TABLE public.prioridad (
                prioridad_id SMALLINT NOT NULL DEFAULT nextval('public.prioridad_prioridad_id_seq'),
                descripcion VARCHAR(20) NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT prioridad_id PRIMARY KEY (prioridad_id)
);
COMMENT ON TABLE public.prioridad IS 'Tabla de registros de las prioridades';
COMMENT ON COLUMN public.prioridad.prioridad_id IS 'PRIMARY KEY Identificador de prioridad';
COMMENT ON COLUMN public.prioridad.descripcion IS 'Descripci�n de la prioridad';
COMMENT ON COLUMN public.prioridad.estado IS 'Estado de la prioridad';


ALTER SEQUENCE public.prioridad_prioridad_id_seq OWNED BY public.prioridad.prioridad_id;

CREATE SEQUENCE public.rol_rol_id_seq;

CREATE TABLE public.rol (
                rol_id SMALLINT NOT NULL DEFAULT nextval('public.rol_rol_id_seq'),
                descripcion VARCHAR(20) NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT rol_id PRIMARY KEY (rol_id)
);
COMMENT ON TABLE public.rol IS 'Tabla de registros de los roles';
COMMENT ON COLUMN public.rol.rol_id IS 'PRIMARY KEY Identificador del rol';
COMMENT ON COLUMN public.rol.descripcion IS 'Descripci�n del rol';
COMMENT ON COLUMN public.rol.estado IS 'Estado del rol';


ALTER SEQUENCE public.rol_rol_id_seq OWNED BY public.rol.rol_id;

CREATE SEQUENCE public.persona_persona_id_seq;

CREATE TABLE public.persona (
                persona_id INTEGER NOT NULL DEFAULT nextval('public.persona_persona_id_seq'),
                cedula VARCHAR(10) NOT NULL,
                nombre VARCHAR(40) NOT NULL,
                apellido VARCHAR(40) NOT NULL,
                telefono VARCHAR(20) NOT NULL,
                email VARCHAR(40) NOT NULL,
                direccion VARCHAR(100) NOT NULL,
                fecha_nacimiento DATE NOT NULL,
                sexo CHAR(1) NOT NULL,
                nacionalidad VARCHAR(20) NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT persona_id PRIMARY KEY (persona_id)
);
COMMENT ON TABLE public.persona IS 'Tabla de registros de las personas';
COMMENT ON COLUMN public.persona.persona_id IS 'PRIMARY KEY Identificador de la persona';
COMMENT ON COLUMN public.persona.cedula IS 'N�mero de documento de identidad de la persona';
COMMENT ON COLUMN public.persona.nombre IS 'Nombre de la persona';
COMMENT ON COLUMN public.persona.apellido IS 'Apellido de la persona';
COMMENT ON COLUMN public.persona.telefono IS 'N�mero de tel�fono de la persona';
COMMENT ON COLUMN public.persona.email IS 'Correo electr�nico de la persona';
COMMENT ON COLUMN public.persona.direccion IS 'Direcci�n particular de la persona';
COMMENT ON COLUMN public.persona.fecha_nacimiento IS 'Fecha de nacimiento de la persona';
COMMENT ON COLUMN public.persona.sexo IS 'Sexo de la persona';
COMMENT ON COLUMN public.persona.nacionalidad IS 'Nacionalidad de la persona';
COMMENT ON COLUMN public.persona.estado IS 'Estado de la persona';


ALTER SEQUENCE public.persona_persona_id_seq OWNED BY public.persona.persona_id;

CREATE SEQUENCE public.personal_personal_id_seq;

CREATE TABLE public.personal (
                personal_id SMALLINT NOT NULL DEFAULT nextval('public.personal_personal_id_seq'),
                persona_id INTEGER NOT NULL,
                rol_id SMALLINT NOT NULL,
                fecha_alta DATE NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT personal_id PRIMARY KEY (personal_id)
);
COMMENT ON TABLE public.personal IS 'Tabla de registros de los personales';
COMMENT ON COLUMN public.personal.personal_id IS 'PRIMARY KEY Identificador del personal';
COMMENT ON COLUMN public.personal.persona_id IS 'FOREIGN KEY Identificador de persona';
COMMENT ON COLUMN public.personal.rol_id IS 'FOREIGN KEY Identificador del rol';
COMMENT ON COLUMN public.personal.fecha_alta IS 'Fecha de incorporaci�n del personal';
COMMENT ON COLUMN public.personal.estado IS 'Estado del personal';


ALTER SEQUENCE public.personal_personal_id_seq OWNED BY public.personal.personal_id;

CREATE SEQUENCE public.caja_caja_id_seq;

CREATE TABLE public.caja (
                caja_id SMALLINT NOT NULL DEFAULT nextval('public.caja_caja_id_seq'),
                descripcion VARCHAR(20) NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                personal_id SMALLINT NOT NULL,
                CONSTRAINT caja_id PRIMARY KEY (caja_id)
);
COMMENT ON TABLE public.caja IS 'Tabla de registros de las cajas';
COMMENT ON COLUMN public.caja.caja_id IS 'PRIMARY KEY Identificador de caja';
COMMENT ON COLUMN public.caja.descripcion IS 'Descripci�n de la caja';
COMMENT ON COLUMN public.caja.estado IS 'Estado de la caja';
COMMENT ON COLUMN public.caja.personal_id IS 'FOREIGN KEY Identificador del personal';


ALTER SEQUENCE public.caja_caja_id_seq OWNED BY public.caja.caja_id;

CREATE SEQUENCE public.tipo_cliente_tipo_cliente_id_seq;

CREATE TABLE public.tipo_cliente (
                tipo_cliente_id SMALLINT NOT NULL DEFAULT nextval('public.tipo_cliente_tipo_cliente_id_seq'),
                descripcion VARCHAR(50) NOT NULL,
                prioridad_id SMALLINT NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT tipo_cliente_id PRIMARY KEY (tipo_cliente_id)
);
COMMENT ON TABLE public.tipo_cliente IS 'Tabla de registros de los tipos de clientes';
COMMENT ON COLUMN public.tipo_cliente.tipo_cliente_id IS 'PRIMARY KEY Identificador del tipo de cliente';
COMMENT ON COLUMN public.tipo_cliente.descripcion IS 'Descripci�n del tipo de cliente';
COMMENT ON COLUMN public.tipo_cliente.prioridad_id IS 'FOREIGN KEY Identificador de prioridad';
COMMENT ON COLUMN public.tipo_cliente.estado IS 'Estado del tipo de cliente';


ALTER SEQUENCE public.tipo_cliente_tipo_cliente_id_seq OWNED BY public.tipo_cliente.tipo_cliente_id;

CREATE SEQUENCE public.servicio_servicio_id_seq;

CREATE TABLE public.servicio (
                servicio_id SMALLINT NOT NULL DEFAULT nextval('public.servicio_servicio_id_seq'),
                descripcion VARCHAR(20) NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT servicio_id PRIMARY KEY (servicio_id)
);
COMMENT ON TABLE public.servicio IS 'Tabla de registros de los servicios';
COMMENT ON COLUMN public.servicio.servicio_id IS 'PRIMARY KEY Identificador del servicio';
COMMENT ON COLUMN public.servicio.descripcion IS 'Descripci�n del servicio';
COMMENT ON COLUMN public.servicio.estado IS 'Estado del servicio';


ALTER SEQUENCE public.servicio_servicio_id_seq OWNED BY public.servicio.servicio_id;

CREATE SEQUENCE public.cola_cola_id_seq;

CREATE TABLE public.cola (
                cola_id INTEGER NOT NULL DEFAULT nextval('public.cola_cola_id_seq'),
                servicio_id SMALLINT NOT NULL,
                caja_id SMALLINT NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT cola_id PRIMARY KEY (cola_id)
);
COMMENT ON TABLE public.cola IS 'Tabla de registro de las colas';
COMMENT ON COLUMN public.cola.cola_id IS 'PRIMARY KEY Identificador de la cola';
COMMENT ON COLUMN public.cola.servicio_id IS 'FOREIGN KEY Identificador del servicio';
COMMENT ON COLUMN public.cola.caja_id IS 'FOREIGN KEY Identificador de caja';
COMMENT ON COLUMN public.cola.estado IS 'Estado de la cola';


ALTER SEQUENCE public.cola_cola_id_seq OWNED BY public.cola.cola_id;

CREATE SEQUENCE public.usuario_usuario_id_seq;

CREATE TABLE public.usuario (
                usuario_id SMALLINT NOT NULL DEFAULT nextval('public.usuario_usuario_id_seq'),
                nombre VARCHAR(20) NOT NULL,
                clave VARCHAR(30) NOT NULL,
                personal_id SMALLINT NOT NULL,
                fecha_alta DATE NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT usuario_id PRIMARY KEY (usuario_id)
);
COMMENT ON TABLE public.usuario IS 'Tabla de registros de los usuarios';
COMMENT ON COLUMN public.usuario.usuario_id IS 'PRIMARY KEY Identificador del usuario';
COMMENT ON COLUMN public.usuario.nombre IS 'Nombre de usuario';
COMMENT ON COLUMN public.usuario.clave IS 'Contrase�a del usuario';
COMMENT ON COLUMN public.usuario.personal_id IS 'FOREIGN KEY Identificador del personal';
COMMENT ON COLUMN public.usuario.fecha_alta IS 'Fecha de creaci�n del usuario';
COMMENT ON COLUMN public.usuario.estado IS 'Estado del usuario';


ALTER SEQUENCE public.usuario_usuario_id_seq OWNED BY public.usuario.usuario_id;

CREATE SEQUENCE public.puente_usuario_permiso_puente_usuario_permiso_id_seq;

CREATE TABLE public.puente_usuario_permiso (
                puente_usuario_permiso_id INTEGER NOT NULL DEFAULT nextval('public.puente_usuario_permiso_puente_usuario_permiso_id_seq'),
                usuario_id SMALLINT NOT NULL,
                permiso_id SMALLINT NOT NULL,
                CONSTRAINT puente_usuario_permiso_id PRIMARY KEY (puente_usuario_permiso_id)
);
COMMENT ON TABLE public.puente_usuario_permiso IS 'Tabla puente para relacionar los usuarios con sus permisos';
COMMENT ON COLUMN public.puente_usuario_permiso.puente_usuario_permiso_id IS 'PRIMARY KEY Identificador del puente usuario-permiso';
COMMENT ON COLUMN public.puente_usuario_permiso.usuario_id IS 'FOREIGN KEY Identificador del usuario';
COMMENT ON COLUMN public.puente_usuario_permiso.permiso_id IS 'FOREIGN KEY Identificador del permiso';


ALTER SEQUENCE public.puente_usuario_permiso_puente_usuario_permiso_id_seq OWNED BY public.puente_usuario_permiso.puente_usuario_permiso_id;

CREATE SEQUENCE public.cliente_cliente_id_seq;

CREATE TABLE public.cliente (
                cliente_id INTEGER NOT NULL DEFAULT nextval('public.cliente_cliente_id_seq'),
                persona_id INTEGER NOT NULL,
                fecha_alta DATE NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                CONSTRAINT cliente_id PRIMARY KEY (cliente_id)
);
COMMENT ON TABLE public.cliente IS 'Tabla de registros de los clientes';
COMMENT ON COLUMN public.cliente.cliente_id IS 'PRIMARY KEY Identificador del cliente';
COMMENT ON COLUMN public.cliente.persona_id IS 'FOREIGN KEY Identificador de la persona';
COMMENT ON COLUMN public.cliente.fecha_alta IS 'Fecha de registro del cliente';
COMMENT ON COLUMN public.cliente.estado IS 'Estado del cliente';


ALTER SEQUENCE public.cliente_cliente_id_seq OWNED BY public.cliente.cliente_id;

CREATE SEQUENCE public.turno_turno_id_seq;

CREATE TABLE public.turno (
                turno_id INTEGER NOT NULL DEFAULT nextval('public.turno_turno_id_seq'),
                cola_id INTEGER NOT NULL,
                cliente_id INTEGER NOT NULL,
                fecha_creacion DATE NOT NULL,
                estado BOOLEAN DEFAULT true NOT NULL,
                nro_turno INTEGER NOT NULL,
                CONSTRAINT turno_id PRIMARY KEY (turno_id)
);
COMMENT ON TABLE public.turno IS 'Tabla de registros de los turnos';
COMMENT ON COLUMN public.turno.turno_id IS 'PRIMARY KEY Identificador del turno';
COMMENT ON COLUMN public.turno.cola_id IS 'PRIMARY KEY Identificador de la cola';
COMMENT ON COLUMN public.turno.cliente_id IS 'PRIMARY KEY Identificador del cliente';
COMMENT ON COLUMN public.turno.fecha_creacion IS 'Fecha de creaci�n del turno';
COMMENT ON COLUMN public.turno.estado IS 'Estado del turno';
COMMENT ON COLUMN public.turno.nro_turno IS 'N�mero del turno';


ALTER SEQUENCE public.turno_turno_id_seq OWNED BY public.turno.turno_id;

CREATE SEQUENCE public.puente_cliente_tipo_puente_cliente_tipo_id_seq;

CREATE TABLE public.puente_cliente_tipo (
                puente_cliente_tipo_id INTEGER NOT NULL DEFAULT nextval('public.puente_cliente_tipo_puente_cliente_tipo_id_seq'),
                cliente_id INTEGER NOT NULL,
                tipo_cliente_id SMALLINT NOT NULL,
                CONSTRAINT puente_cliente_tipo_id PRIMARY KEY (puente_cliente_tipo_id)
);
COMMENT ON TABLE public.puente_cliente_tipo IS 'Tabla puente para relacionar los clientes con los tipos de cliente';
COMMENT ON COLUMN public.puente_cliente_tipo.puente_cliente_tipo_id IS 'PRIMARY KEY Identificador del puente cliente-tipo';
COMMENT ON COLUMN public.puente_cliente_tipo.cliente_id IS 'FOREIGN KEY Identificador del cliente';
COMMENT ON COLUMN public.puente_cliente_tipo.tipo_cliente_id IS 'FOREIGN KEY Identificador del tipo de cliente';


ALTER SEQUENCE public.puente_cliente_tipo_puente_cliente_tipo_id_seq OWNED BY public.puente_cliente_tipo.puente_cliente_tipo_id;

ALTER TABLE public.puente_usuario_permiso ADD CONSTRAINT permiso_puente_usuario_permiso_fk
FOREIGN KEY (permiso_id)
REFERENCES public.permiso (permiso_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tipo_cliente ADD CONSTRAINT prioridad_tipo_cliente_fk
FOREIGN KEY (prioridad_id)
REFERENCES public.prioridad (prioridad_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.personal ADD CONSTRAINT rol_personal_fk
FOREIGN KEY (rol_id)
REFERENCES public.rol (rol_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.personal ADD CONSTRAINT persona_personal_fk
FOREIGN KEY (persona_id)
REFERENCES public.persona (persona_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cliente ADD CONSTRAINT persona_cliente_fk
FOREIGN KEY (persona_id)
REFERENCES public.persona (persona_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario ADD CONSTRAINT personal_usuario_fk
FOREIGN KEY (personal_id)
REFERENCES public.personal (personal_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.caja ADD CONSTRAINT personal_caja_fk
FOREIGN KEY (personal_id)
REFERENCES public.personal (personal_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cola ADD CONSTRAINT caja_cola_fk
FOREIGN KEY (caja_id)
REFERENCES public.caja (caja_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.puente_cliente_tipo ADD CONSTRAINT tipo_cliente_puente_cliente_tipo_fk
FOREIGN KEY (tipo_cliente_id)
REFERENCES public.tipo_cliente (tipo_cliente_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cola ADD CONSTRAINT servicio_cola_fk
FOREIGN KEY (servicio_id)
REFERENCES public.servicio (servicio_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.turno ADD CONSTRAINT cola_turno_fk
FOREIGN KEY (cola_id)
REFERENCES public.cola (cola_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.puente_usuario_permiso ADD CONSTRAINT usuario_puente_usuario_permiso_fk
FOREIGN KEY (usuario_id)
REFERENCES public.usuario (usuario_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.puente_cliente_tipo ADD CONSTRAINT cliente_puente_cliente_tipo_fk
FOREIGN KEY (cliente_id)
REFERENCES public.cliente (cliente_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.turno ADD CONSTRAINT cliente_turno_fk
FOREIGN KEY (cliente_id)
REFERENCES public.cliente (cliente_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

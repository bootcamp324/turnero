from django import forms
from .models import Servicio
        
class RegistrarTurnoForm(forms.Form):
    
    condiciones = (
        ("C", "Cliente personal"),
        ("B", "Cliente corporativo"),
        ("A", "Tercera edad"),
        ("A", "Discapacidad"),
        ("A", "Embarazada")
    )
    
    cliente = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    servicio = forms.ModelChoiceField(Servicio.objects.all(), widget=forms.Select(attrs={'class':'form-select'}))
    condicion = forms.ChoiceField(choices=condiciones, widget=forms.Select(attrs={'class':'form-select'}))
    
class SeleccionarServicioForm(forms.Form):
     
    servicio = forms.ModelChoiceField(Servicio.objects.all(), widget=forms.Select(attrs={'class':'form-select'}))
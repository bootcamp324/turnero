from django.contrib import admin
from .models import *


class PersonaAdmin(admin.ModelAdmin):
    list_display = ["__str__", "persona_id", "cedula", "telefono", "email", "direccion", "fecha_nacimiento", "sexo", "nacionalidad", "estado"]
    list_filter = ["nombre", "apellido"]
    list_editable = ["estado"]
    search_fields = ["nombre", "apellido", "cedula"]
    
    class Meta:
        model = Persona
    
class ClienteAdmin(admin.ModelAdmin):
    list_display = ["__str__", "cliente_id", "tipo_cliente", "fecha_alta", "estado"]
    list_filter = ["persona", "estado", "fecha_alta", "tipo_cliente"]
    list_editable = ["estado"]
    search_fields = ["persona__nombre", "persona__apellido", "cliente_id", "fecha_alta"]
    
    class Meta:
        model = Cliente
    
class ServicioAdmin(admin.ModelAdmin):
    list_display = ["__str__", "servicio_id", "estado"]
    list_filter = ["estado"]
    list_editable = ["estado"]
    search_fields = ["descripcion", "servicio_id", "estado"]
    
    class Meta:
        model = Servicio
    
class RolAdmin(admin.ModelAdmin):
    list_display = ["__str__", "rol_id", "estado"]
    list_filter = ["estado"]
    list_editable = ["estado"]
    search_fields = ["descripcion", "rol_id", "estado"]
    
    class Meta:
        model = Rol
        
# class TurnoAdmin(admin.ModelAdmin):
#     list_display = ["__str__", "prioridad", "nro_turno", "turno_id", "cola", "cliente", "fecha_creacion", "estado"]
#     list_filter = ["prioridad", "cola", "cliente", "fecha_creacion", "estado"]
#     list_editable = ["estado"]
#     search_fields = ["cliente__persona__nombre", "cliente__persona__apellido", "nro_turno", "prioridad"]
    
#     class Meta:
#         model = Turno
        
class PersonalAdmin(admin.ModelAdmin):
    list_display = ["__str__", "personal_id", "rol", "fecha_alta", "estado"]
    list_filter = ["fecha_alta", "estado"]
    list_editable = ["estado"]
    search_fields = ["persona__nombre", "persona__apellido", "rol__descripcion"]
    
class CajaAdmin(admin.ModelAdmin):
    list_display = ["__str__", "caja_id", "personal", "estado"]
    list_filter = ["estado"]
    list_editable = ["estado"]
    search_fields = ["personal__persona__nombre", "personal__persona__apellido"]
    
class ColaAdmin(admin.ModelAdmin):
    list_display = ["__str__", "cola_id", "caja", "estado"]
    list_filter = ["estado"]
    list_editable = ["estado"]
    search_fields = ["servicio__descripcion"]
    
# class PermisoAdmin(admin.ModelAdmin):
#     list_display = ["__str__", "permiso_id", "estado"]
#     list_filter = ["estado"]
#     list_editable = ["estado"]
#     search_fields = ["descripcion"]
    
class TipoClienteAdmin(admin.ModelAdmin):
    list_display = ["__str__", "tipo_cliente_id", "estado"]
    list_filter = ["estado"]
    list_editable = ["estado"]
    search_fields = ["descripcion"]
    
    
admin.site.register(Persona, PersonaAdmin)
admin.site.register(Servicio, ServicioAdmin)
admin.site.register(Rol, RolAdmin)
admin.site.register(Cliente, ClienteAdmin)
# admin.site.register(Turno, TurnoAdmin)
admin.site.register(Personal, PersonalAdmin)
admin.site.register(Caja, CajaAdmin)
admin.site.register(Cola, ColaAdmin)
# admin.site.register(Permiso, PermisoAdmin)
admin.site.register(TipoCliente, TipoClienteAdmin)
# admin.site.register(Usuario)

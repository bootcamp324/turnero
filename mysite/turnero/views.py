from django.shortcuts import render, redirect
from django.db.models import Max
from django.utils import timezone
from .forms import RegistrarTurnoForm, SeleccionarServicioForm
from .models import Cliente, Persona, Cola, Turno
from queue import PriorityQueue
from django.views.generic import ListView


def inicio(request):
    usuario = "usuario"
    if request.user.is_authenticated:  
        usuario = str(request.user)
            
    context = {
        "titulo":"Inicio",
        "usuario":usuario
    }
    return render(request, 'inicio.html', context)

def exito(request):
    return render(request, "exito.html")

def registrarTurno(request):
    form = RegistrarTurnoForm(request.POST or None)
    
    POST_data = request.POST # Obtener los datos del POST
    error = ""
    
    if form.is_valid():  
        if POST_data:

            # Extraer los datos
            ci = POST_data["cliente"]
            servicio = POST_data["servicio"]
            condicion = POST_data["condicion"]
            
             # Buscar una persona con el ci puesto
            if error == "":
                try:
                    persona = Persona.objects.get(cedula=ci) 
                except Persona.DoesNotExist:
                    # manejar el caso en el que el objeto no existe
                    error = "No existe ninguna persona con esa cédula"
                except Persona.MultipleObjectsReturned:
                    # manejar el caso en el que hay múltiples ci
                    error = "Hay múltiples personas con esa cédula"
            
            # Buscar un cliente con el id de la persona
            if error == "":
                try:
                    cliente = Cliente.objects.get(persona_id=persona.pk)
                except Cliente.DoesNotExist:
                    error = "No existe ningun cliente con esa cédula"
                except Persona.MultipleObjectsReturned:
                    error = "Hay múltiples clientes con esa cédula"
                    
            # Buscar una cola con el servicio indicado
            if error == "":
                try:
                    cola = Cola.objects.get(servicio_id=servicio)
                except Cola.DoesNotExist:
                    error = "No existe una cola con ese servicio"
                except Cola.MultipleObjectsReturned:
                    error = "Hay múltiples colas con ese servicio"
                    
            # Buscar los turnos con la misma prioridad que estan en esa cola y obtener el mayor nro de turno
            nro_turno_actual = 1
            if error == "":
                nro_turno_max = Turno.objects.filter(cola_id=cola.pk, prioridad=condicion).aggregate(Max('nro_turno'))['nro_turno__max']
                if(nro_turno_max != None):
                    nro_turno_actual = nro_turno_max + 1
            
            # Guardamos los datos en la base de datos
            if error == "":
                obj = Turno()
                obj.cola_id = cola.pk
                obj.cliente_id = cliente.pk
                obj.fecha_creacion = timezone.now()
                obj.estado = True
                obj.nro_turno = nro_turno_actual
                obj.prioridad = condicion
                obj.save()
                
    context = {
        "form":form,
        "titulo":"Registrar Turno",
        "error":error
    }
    
    return render(request, "registrar-turno.html", context)

def retornarCola(servicio):
    error = ""
    if servicio != None:
        try:
            cola = Cola.objects.get(servicio=servicio)
            if Turno.objects.filter(cola=cola) != None:
                return cola.pk, error
            else:
                error = "No hay turnos en esa cola"
        except Cola.DoesNotExist:
            error = "No existe una cola con ese servicio"
        except Cola.MultipleObjectsReturned:
            error = "Hay más de una cola con ese servicio"
    return None, error
    
def finalizarTurno_Servicio(request):
    
    error = ""
    form_servicio = SeleccionarServicioForm(request.POST or None)
    
    # Procesar los datos
    if request.method == "POST" and error == "":
        servicio_elegido = request.POST.get("servicio")
        cola, error = retornarCola(servicio_elegido)
        if error == "":
            request.session['cola'] = cola

        # Redirigir al usuario a la página para seleccionar el turno
        if error == "":
             return redirect('finalizarTurno_Turno')
    
    context = {
        "titulo":"Editar turno - Seleccionar servicio",
        "paso":"servicio",
        "error":error,
        "form_seleccionar_servicio":form_servicio,
    }
    
    return render(request, "finalizar-turnos.html", context)

def finalizarTurno_Turno(request):
    turnos = None
    error = ""
    cola_id = request.session.get('cola')
    
    if cola_id != None:
        cola = Cola.objects.get(cola_id=cola_id)
        turnos = Turno.objects.filter(cola=cola)
    
    if request.method == 'POST':
        turno = request.POST.get("turno")
        if turno != None:
            try:
                turno = Turno.objects.get(turno_id=turno)
                if turno != None:
                    turno.delete()
            except Turno.DoesNotExist:
                error = "No existe el turno seleccionado"
            except Turno.MultipleObjectsReturned:
                error = "Hay múltiples registros de ese turno"
    
    context = {
        "titlulo":"Editar turno - Seleccionar turno",
        "paso":"turno",
        "turnos":turnos
    }

    return render(request, "finalizar-turnos.html", context)

def mostrarTurnos_Servicio(request):
    
    error = ""
    form_servicio = SeleccionarServicioForm(request.POST or None)
    
    # Procesar los datos
    if request.method == "POST" and error == "":
        servicio_elegido = request.POST.get("servicio")
        cola, error = retornarCola(servicio_elegido)
        if error == "":
            request.session['cola'] = cola

        # Redirigir al usuario a la página para seleccionar el turno
        if error == "":
             return redirect('mostrarTurnos')
    
    context = {
        'titulo':'Mostrar turnos',
        'error':error,
        'paso':'servicio',
        'form_seleccionar_servicio':form_servicio
    }
    
    return render(request, "mostrar-turnos.html", context)

def mostrarTurnos(request):
    
    turnos = None
    error = ""
    cola_id = request.session.get('cola')
    
    if cola_id != None:
        cola = Cola.objects.get(cola_id=cola_id)
        turnos = Turno.objects.filter(cola=cola).order_by('prioridad', 'nro_turno')[:5]
    
    context = {
        "titulo":"Mostrar turnos",
        "error":error,
        "turnos":turnos,
        "paso":"mostrar"
    }
    
    return render(request, 'mostrar-turnos.html', context)
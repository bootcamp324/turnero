# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Servicio(models.Model):
    servicio_id = models.SmallAutoField(primary_key=True)
    descripcion = models.CharField(max_length=20)
    estado = models.BooleanField()
    
    def __str__(self):
        return self.descripcion

    class Meta:
        db_table = 'servicio'


class Persona(models.Model):
    persona_id = models.AutoField(primary_key=True)
    cedula = models.CharField(max_length=10)
    nombre = models.CharField(max_length=40)
    apellido = models.CharField(max_length=40)
    telefono = models.CharField(max_length=20)
    email = models.CharField(max_length=40)
    direccion = models.CharField(max_length=100)
    fecha_nacimiento = models.DateField()
    sexo = models.CharField(max_length=1)
    nacionalidad = models.CharField(max_length=20)
    estado = models.BooleanField()

    def __str__(self):
        return self.nombre + ' ' + self.apellido

    class Meta:
        db_table = 'persona'


class Rol(models.Model):
    rol_id = models.SmallAutoField(primary_key=True)
    descripcion = models.CharField(max_length=20)
    estado = models.BooleanField()
    
    def __str__(self):
        return self.descripcion

    class Meta:
        db_table = 'rol'
        verbose_name_plural = 'roles'


class Personal(models.Model):
    personal_id = models.SmallAutoField(primary_key=True)
    persona = models.OneToOneField(Persona, on_delete=models.CASCADE)
    rol = models.ForeignKey(Rol, on_delete=models.PROTECT)
    fecha_alta = models.DateField()
    estado = models.BooleanField()
    
    def __str__(self):
        return self.persona.__str__()

    class Meta:
        db_table = 'personal'
        verbose_name_plural = 'personales'


class Caja(models.Model):
    caja_id = models.SmallAutoField(primary_key=True)
    descripcion = models.CharField(max_length=20)
    estado = models.BooleanField()
    personal = models.OneToOneField(Personal, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.descripcion

    class Meta:
        db_table = 'caja'
        
        
class TipoCliente(models.Model):
    
    tipo_cliente_id = models.SmallAutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    estado = models.BooleanField()
    
    def __str__(self):
        return self.descripcion

    class Meta:
        db_table = 'tipo_cliente'
        verbose_name = 'tipo de cliente'
        verbose_name_plural = 'tipos de clientes'


class Cliente(models.Model):
    cliente_id = models.AutoField(primary_key=True)
    persona = models.OneToOneField(Persona, on_delete=models.CASCADE)
    tipo_cliente = models.ForeignKey(TipoCliente, on_delete=models.PROTECT)
    fecha_alta = models.DateField()
    estado = models.BooleanField()
    
    def __str__(self):
        return self.persona.__str__()

    class Meta:
        db_table = 'cliente'


class Cola(models.Model):
    cola_id = models.AutoField(primary_key=True)
    servicio = models.OneToOneField(Servicio, on_delete=models.CASCADE)
    caja = models.OneToOneField(Caja, on_delete=models.CASCADE)
    estado = models.BooleanField()
    
    def __str__(self):
        return self.servicio.__str__()

    class Meta:
        db_table = 'cola'


# class Permiso(models.Model):
#     permiso_id = models.SmallAutoField(primary_key=True)
#     descripcion = models.CharField(max_length=20)
#     estado = models.BooleanField()
    
#     def __str__(self):
#         return self.descripcion

#     class Meta:
#         db_table = 'permiso'


# class Usuario(models.Model):
#     usuario_id = models.SmallAutoField(primary_key=True)
#     nombre = models.CharField(max_length=20)
#     clave = models.CharField(max_length=30)
#     personal = models.OneToOneField(Personal, on_delete=models.CASCADE)
#     permisos = models.ManyToManyField(Permiso)
#     fecha_alta = models.DateField()
#     estado = models.BooleanField()
    
#     def __str__(self):
#         return self.nombre

#     class Meta:
#         db_table = 'usuario'


class Turno(models.Model):
    
    PRIORIDAD_CHOICES = (
        ('A', 'Alta'),
        ('B', 'Media'),
        ('C', 'Baja'),
    )
    
    turno_id = models.AutoField(primary_key=True)
    cola = models.ForeignKey(Cola, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    fecha_creacion = models.DateField()
    estado = models.BooleanField()
    nro_turno = models.IntegerField()
    prioridad = models.CharField(max_length=1, choices=PRIORIDAD_CHOICES, default='C')
    
    def __str__(self):
        return str(self.prioridad) + '-' + str(self.nro_turno).zfill(3)

    class Meta:
        db_table = 'turno'